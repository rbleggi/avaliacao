# Observação
Foi utilizado o mesmo repositório para os dois exercícios, onde a resposta do exercício 2 está em `src/main/java/GeradorObservacao.java` junto com o documento original e seu teste está em `src/test/java/GeradorObservavaoTest.java`

# Resposta Exercício 2
Organizar constantes de texto, pode ser colocado no proprio arquivo ou em outro apenas de constantes. Ajuda na leitura e organização da classe.
Utilizar funções mais atuais para concatenação, pois auxilia na organização de código.
Utilizaria stream, map e joining para gerar os números de notas com o mesmo separador.
Utilizaria o String.format para deixar legível a concatenação das strings para a resposta final.
## Observação
Foi utilizado o mesmo repositório para os dois exercícios, onde a resposta do exercício 2 esta em `src/main/java/GeradorObservacao.java` junto com o documento original e seu teste está em `src/test/java/GeradorObservavaoTest.java`.

# Instruções para execução Exercício 1

## Ambiente Docker Kubernetes
Foi adicionado ao gitlab o projeto com integração continua e docker Kubernetes `http://rbleggi-avaliacao.35.224.198.22.nip.io/`

## Ambiente Eclipse
Importar projeto como `Projeto Maven` e executar com `Run as -> Java Application` a classe `src/main/java/br/com/rogerbleggi/softplan/avaliacao/AvaliacaoApplication.java`
A aplicação irá subir na porta 9999 no link `http://localhost:9999/`.

## Ambiente Windows
Caso não tenha a variável de ambiente `JAVA_HOME` setada na máquina, editar o arquivo `setJAVA_HOME.bat` e alterar a seguinte instrução:

`set PATH_JAVA_HOME=`JDKDIR (ex.: set PATH_JAVA_HOME=C:\Program Files\Java\jdk1.8.0_181)
Após executar o arquivo com permissão de administrador.

Para rodar o programa siga alguma das opções:
1. Executar o arquivo `runServer.bat`. ou
2. Executar via cmd o comando `mvnw.cmd spring-boot:run` na pasta raiz do projeto.