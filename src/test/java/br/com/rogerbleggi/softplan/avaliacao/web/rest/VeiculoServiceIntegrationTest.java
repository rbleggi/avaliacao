package br.com.rogerbleggi.softplan.avaliacao.web.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.rogerbleggi.softplan.avaliacao.service.dto.TipoVeiculoDTO;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.VeiculoDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class VeiculoServiceIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
    private EntityManagerFactory entityManagerFactory;
	
	HttpHeaders headers = new HttpHeaders();

	@Before
	public void setUp() {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		em.createNativeQuery("DELETE FROM veiculo").executeUpdate();
		em.getTransaction().commit();
	}

	@Test
	public void deveAdicionarVeiculo() {
		VeiculoDTO novoVeiculo = new VeiculoDTO();
		novoVeiculo.setDescricao("Caminhao 1");
		novoVeiculo.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO = new TipoVeiculoDTO();
		novoTipoVeiculoDTO.setDescricao("teste");
		novoTipoVeiculoDTO.setId(1l);
		novoVeiculo.setTipoVeiculo(novoTipoVeiculoDTO);

		HttpEntity<VeiculoDTO> entity = new HttpEntity<VeiculoDTO>(novoVeiculo, headers);

		ResponseEntity<VeiculoDTO> response = restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity, VeiculoDTO.class);

		VeiculoDTO veiculoDTOAdicionado = response.getBody();

		assertEquals(novoVeiculo.getDescricao(), veiculoDTOAdicionado.getDescricao());
		assertEquals(novoVeiculo.getFator(), veiculoDTOAdicionado.getFator());
		assertEquals(novoVeiculo.getTipoVeiculo().getDescricao(), veiculoDTOAdicionado.getTipoVeiculo().getDescricao());
	}
	
	@Test
	public void deveAdicionarVeiculosEEncontrarTodos() {
		VeiculoDTO novoVeiculo1 = new VeiculoDTO();
		novoVeiculo1.setDescricao("Caminhao 1");
		novoVeiculo1.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO1 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO1.setDescricao("teste1");
		novoTipoVeiculoDTO1.setId(1l);
		novoVeiculo1.setTipoVeiculo(novoTipoVeiculoDTO1);

		VeiculoDTO novoVeiculo2 = new VeiculoDTO();
		novoVeiculo2.setDescricao("Caminhao 2");
		novoVeiculo2.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO2 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO2.setDescricao("teste2");
		novoTipoVeiculoDTO2.setId(1l);
		novoVeiculo2.setTipoVeiculo(novoTipoVeiculoDTO2);
		
		VeiculoDTO novoVeiculo3 = new VeiculoDTO();
		novoVeiculo3.setDescricao("Caminhao 3");
		novoVeiculo3.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO3 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO3.setDescricao("teste3");
		novoTipoVeiculoDTO3.setId(1l);
		novoVeiculo3.setTipoVeiculo(novoTipoVeiculoDTO3);

		HttpEntity<VeiculoDTO> entity1 = new HttpEntity<VeiculoDTO>(novoVeiculo1, headers);
		HttpEntity<VeiculoDTO> entity2 = new HttpEntity<VeiculoDTO>(novoVeiculo2, headers);
		HttpEntity<VeiculoDTO> entity3 = new HttpEntity<VeiculoDTO>(novoVeiculo3, headers);

		restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity1, VeiculoDTO.class);
		restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity2, VeiculoDTO.class);
		restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity3, VeiculoDTO.class);

		String url = "/api/v1/veiculos/";
		ResponseEntity<List> todosVeiculos = restTemplate.getForEntity(url, List.class);

		List list = todosVeiculos.getBody();

		assertEquals(HttpStatus.OK, todosVeiculos.getStatusCode());
		assertEquals(3, todosVeiculos.getBody().size());
	}

	@Test
	public void deveAdicionarEAcharVeiculo() {
		VeiculoDTO novoVeiculo = new VeiculoDTO();
		novoVeiculo.setDescricao("Caminhao 3");
		novoVeiculo.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO = new TipoVeiculoDTO();
		novoTipoVeiculoDTO.setDescricao("teste3");
		novoTipoVeiculoDTO.setId(1l);
		novoVeiculo.setTipoVeiculo(novoTipoVeiculoDTO);

		HttpEntity<VeiculoDTO> entity = new HttpEntity<VeiculoDTO>(novoVeiculo, headers);

		ResponseEntity<VeiculoDTO> response = restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity, VeiculoDTO.class);

		VeiculoDTO veiculoDTOAdicionado = response.getBody();

		// FindByID
		String url = String.format("/api/v1/veiculos/%d", veiculoDTOAdicionado.getId());
		ResponseEntity<VeiculoDTO> responseDeFindById = restTemplate.getForEntity(url, VeiculoDTO.class);

		VeiculoDTO veiculoDTO = response.getBody();
		assertEquals(HttpStatus.OK, responseDeFindById.getStatusCode());
		assertEquals(veiculoDTOAdicionado.getDescricao(), veiculoDTO.getDescricao());
		assertEquals(veiculoDTOAdicionado.getFator(), veiculoDTO.getFator());
		assertEquals(veiculoDTOAdicionado.getTipoVeiculo().getDescricao(), veiculoDTO.getTipoVeiculo().getDescricao());
	}
	
	@Test
	public void deveAdicionarVeiculosEFiltarPelaDescricao() {
		VeiculoDTO novoVeiculo1 = new VeiculoDTO();
		novoVeiculo1.setDescricao("Caminhao");
		novoVeiculo1.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO1 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO1.setDescricao("teste1");
		novoTipoVeiculoDTO1.setId(1l);
		novoVeiculo1.setTipoVeiculo(novoTipoVeiculoDTO1);

		VeiculoDTO novoVeiculo2 = new VeiculoDTO();
		novoVeiculo2.setDescricao("Caminhao");
		novoVeiculo2.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO2 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO2.setDescricao("teste2");
		novoTipoVeiculoDTO2.setId(1l);
		novoVeiculo2.setTipoVeiculo(novoTipoVeiculoDTO2);
		
		VeiculoDTO novoVeiculo3 = new VeiculoDTO();
		novoVeiculo3.setDescricao("carro");
		novoVeiculo3.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO3 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO3.setDescricao("teste3");
		novoTipoVeiculoDTO3.setId(1l);
		novoVeiculo3.setTipoVeiculo(novoTipoVeiculoDTO3);

		HttpEntity<VeiculoDTO> entity = new HttpEntity<VeiculoDTO>(novoVeiculo1, headers);
		HttpEntity<VeiculoDTO> entity2 = new HttpEntity<VeiculoDTO>(novoVeiculo2, headers);
		HttpEntity<VeiculoDTO> entity3 = new HttpEntity<VeiculoDTO>(novoVeiculo3, headers);

		ResponseEntity<VeiculoDTO> response = restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity, VeiculoDTO.class);
		restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity2, VeiculoDTO.class);
		restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity3, VeiculoDTO.class);

		VeiculoDTO veiculoDTOAdicionado = response.getBody();

		String url = String.format("/api/v1/veiculos/?descricao=%s", veiculoDTOAdicionado.getDescricao());
		ResponseEntity<String> responseFromFindByDescricao = restTemplate.getForEntity(url, String.class);

		String body = responseFromFindByDescricao.getBody();
		int veiculosEncontradoSize = 0;
		Iterator<JsonNode> veiculosEncontradoIterator;
		try {
			veiculosEncontradoIterator = new ObjectMapper().readTree(body).iterator();
			while (veiculosEncontradoIterator.hasNext()) {
				veiculosEncontradoSize++;
				JsonNode veiculoEncontrado = veiculosEncontradoIterator.next();
				assertEquals(veiculoDTOAdicionado.getDescricao(), veiculoEncontrado.get("descricao").textValue());
			}
		} catch (IOException e) {
			fail("erro ao buscar lista para json");
		}

		assertEquals(HttpStatus.OK, responseFromFindByDescricao.getStatusCode());
		assertEquals(2, veiculosEncontradoSize);
	}
	
	@Test
	public void deveAdicionarVeiculosEFiltarPeloFator() {
		VeiculoDTO novoVeiculo1 = new VeiculoDTO();
		novoVeiculo1.setDescricao("Caminhao");
		novoVeiculo1.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO1 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO1.setDescricao("teste1");
		novoTipoVeiculoDTO1.setId(1l);
		novoVeiculo1.setTipoVeiculo(novoTipoVeiculoDTO1);
		
		VeiculoDTO novoVeiculo2 = new VeiculoDTO();
		novoVeiculo2.setDescricao("Caminhao");
		novoVeiculo2.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO2 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO2.setDescricao("teste2");
		novoTipoVeiculoDTO2.setId(1l);
		novoVeiculo2.setTipoVeiculo(novoTipoVeiculoDTO2);
		
		VeiculoDTO novoVeiculo3 = new VeiculoDTO();
		novoVeiculo3.setDescricao("carro");
		novoVeiculo3.setFator(1.3d);
		TipoVeiculoDTO novoTipoVeiculoDTO3 = new TipoVeiculoDTO();
		novoTipoVeiculoDTO3.setDescricao("teste3");
		novoTipoVeiculoDTO3.setId(1l);
		novoVeiculo3.setTipoVeiculo(novoTipoVeiculoDTO3);
		
		HttpEntity<VeiculoDTO> entity = new HttpEntity<VeiculoDTO>(novoVeiculo1, headers);
		HttpEntity<VeiculoDTO> entity2 = new HttpEntity<VeiculoDTO>(novoVeiculo2, headers);
		HttpEntity<VeiculoDTO> entity3 = new HttpEntity<VeiculoDTO>(novoVeiculo3, headers);
		
		ResponseEntity<VeiculoDTO> response = restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity, VeiculoDTO.class);
		restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity2, VeiculoDTO.class);
		restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity3, VeiculoDTO.class);
		
		VeiculoDTO veiculoDTOAdicionado = response.getBody();
		
		String url = String.format("/api/v1/veiculos/?fator=%s", veiculoDTOAdicionado.getFator());
		ResponseEntity<String> responseFromFindByDescricao = restTemplate.getForEntity(url, String.class);
		
		String body = responseFromFindByDescricao.getBody();
		int veiculosEncontradoSize = 0;
		Iterator<JsonNode> veiculosEncontradoIterator;
		try {
			veiculosEncontradoIterator = new ObjectMapper().readTree(body).iterator();
			while (veiculosEncontradoIterator.hasNext()) {
				veiculosEncontradoSize++;
				JsonNode veiculoEncontrado = veiculosEncontradoIterator.next();
				assertEquals(veiculoDTOAdicionado.getFator(), new Double(veiculoEncontrado.get("fator").doubleValue()));
			}
		} catch (IOException e) {
			fail("erro ao buscar lista para json");
		}
		
		assertEquals(HttpStatus.OK, responseFromFindByDescricao.getStatusCode());
		assertEquals(2, veiculosEncontradoSize);
	}
	
	@Test
	public void deveAdicionarERemoverVeiculoPeloId() {
		VeiculoDTO novoVeiculo = new VeiculoDTO();
		novoVeiculo.setDescricao("Caminhao 1");
		novoVeiculo.setFator(1.2d);
		TipoVeiculoDTO novoTipoVeiculoDTO = new TipoVeiculoDTO();
		novoTipoVeiculoDTO.setDescricao("teste");
		novoTipoVeiculoDTO.setId(1l);
		novoVeiculo.setTipoVeiculo(novoTipoVeiculoDTO);

		HttpEntity<VeiculoDTO> entity = new HttpEntity<VeiculoDTO>(novoVeiculo, headers);

		ResponseEntity<VeiculoDTO> response = restTemplate.exchange("/api/v1/veiculos/", HttpMethod.POST, entity, VeiculoDTO.class);

		VeiculoDTO veiculoDTOAdicionado = response.getBody();

		String urlDelete = String.format("/api/v1/veiculos/%d", veiculoDTOAdicionado.getId());
		ResponseEntity<String> responseDeDelete = restTemplate.exchange(urlDelete, HttpMethod.DELETE, null, String.class);

		HttpStatus responseContent = responseDeDelete.getStatusCode();
		assertEquals(HttpStatus.OK, responseContent);
	}

	@Test
	public void deveRetornonarStatusNotFoundTentandoAcharVeiculoNaoExistente() {
		String url = String.format("/api/v1/veiculos/%d", 111111111);
		ResponseEntity<VeiculoDTO> responseDeFindById = restTemplate.getForEntity(url, VeiculoDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, responseDeFindById.getStatusCode());
	}
}
