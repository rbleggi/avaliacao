package br.com.rogerbleggi.softplan.avaliacao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.rogerbleggi.softplan.avaliacao.web.AvaliacaoController;
import br.com.rogerbleggi.softplan.avaliacao.web.rest.CalculoResource;
import br.com.rogerbleggi.softplan.avaliacao.web.rest.VeiculoResource;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AvaliacaoApplicationTests {

	@Autowired
	private AvaliacaoController avaliacaoController;
	
	@Autowired
	private CalculoResource calculoResource;
	
	@Autowired
	private VeiculoResource veiculoResource;

	@Test
	public void contextLoads() {
		assertThat(avaliacaoController).isNotNull();
		assertThat(calculoResource).isNotNull();
		assertThat(veiculoResource).isNotNull();
	}

}
