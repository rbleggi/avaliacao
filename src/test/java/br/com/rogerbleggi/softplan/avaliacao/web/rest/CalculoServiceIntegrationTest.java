package br.com.rogerbleggi.softplan.avaliacao.web.rest;

import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.rogerbleggi.softplan.avaliacao.service.VeiculoService;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.InformacoesCalculoDTO;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.TipoVeiculoDTO;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.VeiculoDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CalculoServiceIntegrationTest {

	@Autowired
	private VeiculoService veiculoService;
	
	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	HttpHeaders headers = new HttpHeaders();
	
	@Before
	public void setUp() {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		em.createNativeQuery("DELETE FROM veiculo").executeUpdate();
		em.getTransaction().commit();
	}
	
	@Test
	public void deveCalcular() {
		VeiculoDTO veiculoDTO = new VeiculoDTO();
		veiculoDTO.setDescricao("Caminhão caçamba");
		veiculoDTO.setFator(1.05);
		TipoVeiculoDTO tipoVeiculoDTO = new TipoVeiculoDTO();
		tipoVeiculoDTO.setDescricao("CAMINHAO");
		tipoVeiculoDTO.setId(1l);
		veiculoDTO.setTipoVeiculo(tipoVeiculoDTO);
		VeiculoDTO veiculoSalvo = veiculoService.save(veiculoDTO);
		
		InformacoesCalculoDTO informacoesCalculo = new InformacoesCalculoDTO();
		informacoesCalculo.setCargaTransportada(8);
		informacoesCalculo.setDistanciaNaoPavimentada(0);
		informacoesCalculo.setDistanciaPavimentada(100);
		informacoesCalculo.setIdVeiculoUtilizado(veiculoService.findOne(veiculoSalvo.getId()).getId());
		
		HttpEntity<InformacoesCalculoDTO> entity = new HttpEntity<InformacoesCalculoDTO>(informacoesCalculo, headers);

		ResponseEntity<InformacoesCalculoDTO> response = restTemplate.exchange("/api/v1/actions/calcular/", HttpMethod.POST, entity, InformacoesCalculoDTO.class);

		InformacoesCalculoDTO resposta = response.getBody();

		assertThat(resposta.getResultado()).isEqualTo(new Double(62.7d));
	}

}
