import java.util.List;
import java.util.stream.Collectors;

public class GeradorObservacao {

	/**
	 * Constantes organizadas
	 */
	static final String UMA_NOTA = "Fatura da nota fiscal de simples remessa:";
	static final String VARIAS_NOTAS = "Fatura das notas fiscais de simples remessa:";

	/** 
	 * Utilizado stream, map, join e format para completar e deixar mais claro a leitura da Observacao. 
	 * Utiliza menos codigo, e deixa mais simples a leitura do metodo.
	 */
	public String geraObservacao(List<Integer> lista) {
		if (!lista.isEmpty()) {
			String ultimoElemento = lista.get(lista.size() - 1).toString();
			if (lista.size() == 1) {
				return String.format("%s %s.", UMA_NOTA, ultimoElemento);
			} else {
				String elementosMeio = lista.stream().limit(lista.size() - 1).map(Object::toString).collect(Collectors.joining(", "));
				return String.format("%s %s e %s.", VARIAS_NOTAS, elementosMeio, ultimoElemento);
			}
		}
		return "";
	}

}