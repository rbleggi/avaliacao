package br.com.rogerbleggi.softplan.avaliacao.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.rogerbleggi.softplan.avaliacao.domain.Veiculo;
import br.com.rogerbleggi.softplan.avaliacao.exception.ResourceNotFoundException;
import br.com.rogerbleggi.softplan.avaliacao.repository.VeiculoRepository;
import br.com.rogerbleggi.softplan.avaliacao.service.VeiculoService;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.VeiculoDTO;
import br.com.rogerbleggi.softplan.avaliacao.service.mapper.VeiculoMapper;

@Service
@Transactional
public class VeiculoServiceImpl implements VeiculoService {
	
	@Autowired
	private VeiculoRepository veiculoRepository;

	@Override
	public VeiculoDTO save(VeiculoDTO veiculoDTO) {
		return VeiculoMapper.converterParaVeiculoDTO(veiculoRepository.save(VeiculoMapper.converterParaVeiculo(veiculoDTO)));
	}

	@Override
	@Transactional(readOnly = true)
	public List<VeiculoDTO> filterVeiculos(VeiculoDTO veiculoDTO) {
		if (veiculoDTO == null) {
			return veiculoRepository.findAll().stream().map(veiculo -> VeiculoMapper.converterParaVeiculoDTO(veiculo)).collect(Collectors.toList());
		} else {
			ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnoreNullValues().withIgnorePaths("id");
			return veiculoRepository.findAll(Example.of(VeiculoMapper.converterParaVeiculo(veiculoDTO), matcher)).stream().map(veiculo -> VeiculoMapper.converterParaVeiculoDTO(veiculo)).collect(Collectors.toList());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public VeiculoDTO findOne(Long id) {
		return VeiculoMapper.converterParaVeiculoDTO(veiculoRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Veiculo", "id", id)));
	}

	@Override
	public void delete(Long id) {
		veiculoRepository.deleteById(id);
	}

	@Override
	public VeiculoDTO update(Long veiculoId, VeiculoDTO veiculoDTO) {
		Veiculo veiculo = veiculoRepository.findById(veiculoId).orElseThrow(() -> new ResourceNotFoundException("Veiculo", "id", veiculoId));
		veiculo.setDescricao(veiculoDTO.getDescricao());
		veiculo.setFator(veiculoDTO.getFator());
		veiculo.getTipoVeiculo().setDescricao(veiculoDTO.getTipoVeiculo().getDescricao());
		return VeiculoMapper.converterParaVeiculoDTO(veiculo);
	}

}
