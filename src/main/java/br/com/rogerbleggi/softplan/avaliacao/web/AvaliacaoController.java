package br.com.rogerbleggi.softplan.avaliacao.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.rogerbleggi.softplan.avaliacao.service.CalculoService;
import br.com.rogerbleggi.softplan.avaliacao.service.VeiculoService;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.InformacoesCalculoDTO;

@Controller
public class AvaliacaoController {

	@Autowired
	private CalculoService calculoService;
	
	@Autowired
	private VeiculoService veiculoService;

	@GetMapping("rest/calculo")
	public String calculoRestForm() {
		return "redirect:/rest/calculo.html";
	}
	
	@GetMapping("servlet/calculo")
	public String calculoServletView(Model model) {
		model.addAttribute("informacoesCalculoDTO", new InformacoesCalculoDTO());
		model.addAttribute("veiculos", veiculoService.filterVeiculos(null));
		return "servlet/calculo";
	}

	@PostMapping("servlet/calculo")
	public String calculoServletSubmit(@Valid @ModelAttribute InformacoesCalculoDTO informacoesCalculoDTO, BindingResult bindingResult, Model model) {
		model.addAttribute("veiculos", veiculoService.filterVeiculos(null));
		if (bindingResult.hasErrors()) {
			return "servlet/calculo";
		}
		informacoesCalculoDTO.setResultado(calculoService.calcularCustoTotalTransporte(informacoesCalculoDTO));
		return "servlet/calculo";
	}

}
