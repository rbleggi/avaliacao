package br.com.rogerbleggi.softplan.avaliacao.service;

import java.util.List;

import br.com.rogerbleggi.softplan.avaliacao.service.dto.VeiculoDTO;

public interface VeiculoService {

	public VeiculoDTO save(VeiculoDTO veiculoDTO);

	public List<VeiculoDTO> filterVeiculos(VeiculoDTO veiculoDTO);

	public VeiculoDTO findOne(Long id);

	public void delete(Long id);

	public VeiculoDTO update(Long veiculoId, VeiculoDTO veiculoDTO);

}