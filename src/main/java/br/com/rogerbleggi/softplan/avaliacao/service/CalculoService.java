package br.com.rogerbleggi.softplan.avaliacao.service;

import br.com.rogerbleggi.softplan.avaliacao.service.dto.InformacoesCalculoDTO;

public interface CalculoService {

	public Double calcularCustoTotalTransporte(InformacoesCalculoDTO calculo);
	
}
