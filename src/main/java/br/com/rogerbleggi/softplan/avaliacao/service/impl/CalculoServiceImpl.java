package br.com.rogerbleggi.softplan.avaliacao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rogerbleggi.softplan.avaliacao.domain.EnumVia;
import br.com.rogerbleggi.softplan.avaliacao.service.CalculoService;
import br.com.rogerbleggi.softplan.avaliacao.service.VeiculoService;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.InformacoesCalculoDTO;

@Service
public class CalculoServiceImpl implements CalculoService {

	private static final Integer LIMTE_CARGA_EXCEDENTE = 5;
	private final Double CUSTO_FIXO_EXCEDENTE = 0.02d;
	
	@Autowired
	private VeiculoService veiculoService;

	@Override
	public Double calcularCustoTotalTransporte(InformacoesCalculoDTO infoCalculo) {
		Integer distanciaPavimentada = infoCalculo.getDistanciaPavimentada();
		Integer distanciaNaoPavimentada = infoCalculo.getDistanciaNaoPavimentada();
		Integer carga = infoCalculo.getCargaTransportada();
		Double fatorVeiculo = veiculoService.findOne(infoCalculo.getIdVeiculoUtilizado()).getFator();

		Double custaTotalViaPavimentado = calculaTotalVia(distanciaPavimentada, fatorVeiculo, EnumVia.PAVIMENTADA.getCusto());
		Double custaTotalViaNaoPavimentado = calculaTotalVia(distanciaNaoPavimentada, fatorVeiculo, EnumVia.NAO_PAVIMENTADA.getCusto());
		Double custaExcedente = calculaCargaExcedente(distanciaPavimentada, distanciaNaoPavimentada, carga);

		return custaTotalViaPavimentado + custaTotalViaNaoPavimentado + custaExcedente;
	}

	private Double calculaCargaExcedente(Integer distanciaPavimentada, Integer distanciaNaoPavimentada, Integer carga) {
		if (carga > LIMTE_CARGA_EXCEDENTE) {
			Integer cargaExcedente = carga - 5;
			Double custoCargaExcedentePavimentado = calculaCustoCargaExcedente(cargaExcedente, distanciaPavimentada);
			Double custoCargaExcedenteNaoPavimentado = calculaCustoCargaExcedente(cargaExcedente, distanciaNaoPavimentada);
			return custoCargaExcedentePavimentado + custoCargaExcedenteNaoPavimentado;
		}
		return 0d;
	}

	private Double calculaTotalVia(Integer distancia, Double fator, Double custoVia) {
		return distancia * fator * custoVia;
	}

	private Double calculaCustoCargaExcedente(Integer cargaExcedente, Integer distancia) {
		return CUSTO_FIXO_EXCEDENTE * cargaExcedente * distancia;
	}

}
