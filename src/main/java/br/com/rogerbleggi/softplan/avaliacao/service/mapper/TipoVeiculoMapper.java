package br.com.rogerbleggi.softplan.avaliacao.service.mapper;

import br.com.rogerbleggi.softplan.avaliacao.domain.TipoVeiculo;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.TipoVeiculoDTO;

public class TipoVeiculoMapper {

	public static TipoVeiculo converterParaTipoVeiculo(TipoVeiculoDTO tipoVeiculoDTO) {
		TipoVeiculo tipoVeiculo = new TipoVeiculo();
		tipoVeiculo.setId(tipoVeiculoDTO.getId());
		tipoVeiculo.setDescricao(tipoVeiculoDTO.getDescricao());
		return tipoVeiculo;
	}

	public static TipoVeiculoDTO converterParaTipoVeiculoDTO(TipoVeiculo tipoVeiculo) {
		TipoVeiculoDTO tipoVeiculoDTO = new TipoVeiculoDTO();
		tipoVeiculoDTO.setId(tipoVeiculo.getId());
		tipoVeiculoDTO.setDescricao(tipoVeiculo.getDescricao());
		return tipoVeiculoDTO;
	}

}
