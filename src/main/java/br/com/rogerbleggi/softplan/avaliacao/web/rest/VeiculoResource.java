package br.com.rogerbleggi.softplan.avaliacao.web.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rogerbleggi.softplan.avaliacao.service.VeiculoService;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.VeiculoDTO;

@RestController
@CrossOrigin
@RequestMapping("api/v1/veiculos")
public class VeiculoResource {

	@Autowired
	private VeiculoService veiculoService;

	@PostMapping
	public VeiculoDTO save(@Valid @RequestBody VeiculoDTO veiculoDTO) {
		return veiculoService.save(veiculoDTO);
	}

	@GetMapping
	public List<VeiculoDTO> filterVeiculos(VeiculoDTO veiculoDTO) {
		return veiculoService.filterVeiculos(veiculoDTO);
	}

	@GetMapping("/{id}")
	public VeiculoDTO getVeiculoById(@PathVariable(value = "id") Long veiculoId) {
		return veiculoService.findOne(veiculoId);
	}

	@PutMapping("/{id}")
	public VeiculoDTO updateVeiculo(@PathVariable(value = "id") Long veiculoId, @Valid @RequestBody VeiculoDTO veiculoDTO) {
		return veiculoService.update(veiculoId, veiculoDTO);
	}

	@DeleteMapping("/{id}")
	public void deleteVeiculoById(@PathVariable(value = "id") Long veiculoId) {
		veiculoService.delete(veiculoId);
	}

}