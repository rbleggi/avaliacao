package br.com.rogerbleggi.softplan.avaliacao.service.dto;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class VeiculoDTO {
	
	@Id
	private Long id;

	private String descricao;

	private Double fator;

	private TipoVeiculoDTO tipoVeiculo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getFator() {
		return fator;
	}

	public void setFator(Double fator) {
		this.fator = fator;
	}

	public TipoVeiculoDTO getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculoDTO tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

}
