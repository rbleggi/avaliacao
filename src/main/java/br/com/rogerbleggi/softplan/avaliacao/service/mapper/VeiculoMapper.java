package br.com.rogerbleggi.softplan.avaliacao.service.mapper;

import br.com.rogerbleggi.softplan.avaliacao.domain.Veiculo;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.VeiculoDTO;

public class VeiculoMapper {

	public static Veiculo converterParaVeiculo(VeiculoDTO veiculoDTO) {
		Veiculo veiculo = new Veiculo();
		veiculo.setDescricao(veiculoDTO.getDescricao());
		veiculo.setFator(veiculoDTO.getFator());
		if (veiculoDTO.getTipoVeiculo() != null)
			veiculo.setTipoVeiculo(TipoVeiculoMapper.converterParaTipoVeiculo(veiculoDTO.getTipoVeiculo()));
		return veiculo;
	}

	public static VeiculoDTO converterParaVeiculoDTO(Veiculo veiculo) {
		VeiculoDTO veiculoDTO = new VeiculoDTO();
		veiculoDTO.setId(veiculo.getId());
		veiculoDTO.setDescricao(veiculo.getDescricao());
		veiculoDTO.setFator(veiculo.getFator());
		veiculoDTO.setTipoVeiculo(TipoVeiculoMapper.converterParaTipoVeiculoDTO(veiculo.getTipoVeiculo()));
		return veiculoDTO;
	}

}
