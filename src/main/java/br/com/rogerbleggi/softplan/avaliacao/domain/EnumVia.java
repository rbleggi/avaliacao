package br.com.rogerbleggi.softplan.avaliacao.domain;

public enum EnumVia {

	PAVIMENTADA(0.54d), NAO_PAVIMENTADA(0.62d);

	private Double custo;

	EnumVia(Double custo) {
		this.custo = custo;
	}

	public Double getCusto() {
		return custo;
	}
}
