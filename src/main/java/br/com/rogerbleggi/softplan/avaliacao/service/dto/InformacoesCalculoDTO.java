package br.com.rogerbleggi.softplan.avaliacao.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InformacoesCalculoDTO {

	@PositiveOrZero
	@NotNull
	private Integer distanciaPavimentada;

	@PositiveOrZero
	@NotNull
	private Integer distanciaNaoPavimentada;

	@NotNull
	private Long idVeiculoUtilizado;

	@PositiveOrZero
	@NotNull
	private Integer cargaTransportada;

	private Double resultado;

	public Integer getDistanciaPavimentada() {
		return distanciaPavimentada;
	}

	public void setDistanciaPavimentada(Integer distanciaPavimentada) {
		this.distanciaPavimentada = distanciaPavimentada;
	}

	public Integer getDistanciaNaoPavimentada() {
		return distanciaNaoPavimentada;
	}

	public void setDistanciaNaoPavimentada(Integer distanciaNaoPavimentada) {
		this.distanciaNaoPavimentada = distanciaNaoPavimentada;
	}

	public Long getIdVeiculoUtilizado() {
		return idVeiculoUtilizado;
	}

	public void setIdVeiculoUtilizado(Long idVeiculoUtilizado) {
		this.idVeiculoUtilizado = idVeiculoUtilizado;
	}

	public Integer getCargaTransportada() {
		return cargaTransportada;
	}

	public void setCargaTransportada(Integer cargaTransportada) {
		this.cargaTransportada = cargaTransportada;
	}

	public Double getResultado() {
		return resultado;
	}

	public void setResultado(Double resultado) {
		this.resultado = resultado;
	}

}
