package br.com.rogerbleggi.softplan.avaliacao.web.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.rogerbleggi.softplan.avaliacao.service.CalculoService;
import br.com.rogerbleggi.softplan.avaliacao.service.dto.InformacoesCalculoDTO;

@RestController
@CrossOrigin
public class CalculoResource {

	@Autowired
	private CalculoService calculoService;

	@PostMapping("api/v1/actions/calcular")
	public InformacoesCalculoDTO calculoSubmit(@Valid @RequestBody InformacoesCalculoDTO informacoesCalculo) {
		informacoesCalculo.setResultado(calculoService.calcularCustoTotalTransporte(informacoesCalculo));
		return informacoesCalculo;
	}

}