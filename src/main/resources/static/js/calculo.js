var ready = (fn) => {
	if (document.readyState === 'complete')
		return fn();
	document.addEventListener('DOMContentLoaded', fn, false);
};

ready(() => {
	buscaVeiculos();
	document.getElementById('calculoForm').addEventListener('submit', function(event) {
		event.preventDefault();
		calculoSubmit();
	});
});

function buscaVeiculos() {
	let xhr = new XMLHttpRequest();
	xhr.open('GET', '/api/v1/veiculos');
	xhr.send();
	xhr.onload = () => {
		document.getElementById('veiculos').innerHTML = JSON.parse(xhr.responseText).map(veiculo => `<option value="${veiculo.id}">${veiculo.descricao}</option>`);
	};
}

function calculoSubmit() {
	let informacoesCalculoDTO = {
		distanciaPavimentada : document.getElementById('distanciaPavimentada').value,
		distanciaNaoPavimentada : document.getElementById('distanciaNaoPavimentada').value,
		idVeiculoUtilizado : document.getElementById('veiculos').value,
		cargaTransportada : document.getElementById('cargaTransportada').value
	}

	document.getElementById('btCalcular').setAttribute('disabled', true);

	let xhr = new XMLHttpRequest();
	xhr.open('POST', '/api/v1/actions/calcular');
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(informacoesCalculoDTO));
	xhr.onload = () => {
		if (xhr.status === 200)
			document.getElementById('resultado').innerHTML = JSON.parse(xhr.responseText).resultado.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
			
		document.getElementById('btCalcular').removeAttribute('disabled');
	};
}
