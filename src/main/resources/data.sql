INSERT INTO tipo_veiculo (id, descricao) VALUES
  (1,'CAMINHAO'),
  (2,'CARRETA');

INSERT INTO veiculo (id, descricao, tipo_veiculo, fator) VALUES
  (1, 'Caminhão Baú', 1, 1),
  (2, 'Caminhão Caçamba', 1, 1.05),
  (3, 'Carreta', 2, 1.12);